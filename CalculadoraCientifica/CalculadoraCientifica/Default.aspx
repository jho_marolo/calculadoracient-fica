﻿<%@ Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CalculadoraCientifica._Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <script>

     
 </script>
   

  <asp:UpdatePanel runat="server" ID="updpnl">
      <ContentTemplate>
      <section class="featured">
        <div class="content-wrapper">
            <div style="padding-left:13.8%">
           
              <asp:TextBox ToolTip="resultados" CssClass="txtprincipal" dir="rtl" Width="51.1%" BackColor="Chocolate" runat="server" ID="textBox1"> </asp:TextBox>
            
                <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="radiolist" CssClass="radio" >
                    <asp:ListItem Text="Rad" Value="Gra" />
                    <asp:ListItem Text="Gra" Value="Gra" Selected="True" />
                </asp:RadioButtonList>
            </div>
             <p style="padding-left:13.8%">
                <asp:Button ID="button31" Width="128" runat="server" Text="Backspace" OnClick="button31_Click"/>
                 <asp:Button ID="button24" runat="server" Text="sin" OnClick="button24_Click" />
                 <asp:Button ID="button25" runat="server" Text="cos" OnClick="button25_Click" />
                 <asp:Button ID="button26" runat="server" Text="tan" OnClick="button26_Click" />
                 <asp:Button ID="button35" Width="55"  runat="server" Text="sin-1" OnClick="button35_Click" />
                 <asp:Button ID="button34" Width="55" runat="server" Text="cos-1" OnClick="button34_Click" />
                 <asp:Button ID="button36" Width="55"  runat="server" Text="tan-1" OnClick="button36_Click" />
            </p>

                <p style="padding-left:13.8%">
                <asp:Button ID="button4" runat="server" Text="Pi" OnClick="button4_Click" />
                   <asp:Button ID="button16" runat="server" Text="7" OnClick="button16_Click" />
                   <asp:Button ID="button17" runat="server" Text="8" OnClick="button17_Click" />
                   <asp:Button ID="button18" runat="server" Text="9" OnClick="button18_Click" />
                   <asp:Button ID="button15" runat="server" Text="/" OnClick="button15_Click" />
                   <asp:Button ID="button28" runat="server" Text="x!" OnClick="button28_Click" />
                   <asp:Button ID="button37" runat="server" Text="nPr" OnClick="button37_Click" />
                   <asp:Button ID="button38" runat="server" Text="nCr" OnClick="button38_Click" />
                 
                 </p>
                 <p style="padding-left:13.8%">
                <asp:Button ID="button39" runat="server" Text="OFF" OnClick="button39_Click" />
                  <asp:Button ID="button11" runat="server" Text="4" OnClick="button11_Click" />
                  <asp:Button ID="button12" runat="server" Text="5" OnClick="button12_Click" />
                  <asp:Button ID="button13" runat="server" Text="6" OnClick="button13_Click" />
                  <asp:Button ID="button10" runat="server" Text="*" OnClick="button10_Click" />
                  <asp:Button ID="button19" runat="server" Text="x²" OnClick="button19_Click" />
                  <asp:Button ID="button20" runat="server" Text="x³" OnClick="button20_Click" />
                  <asp:Button ID="button22" runat="server" Text="x^y" OnClick="button22_Click" />
                 </p>
                <p style="padding-left:13.8%">
                 <asp:Button ID="button23" runat="server" Text="CE" OnClick="button23_Click" />
                 <asp:Button ID="button6" runat="server" Text="1" OnClick="button6_Click" />
                 <asp:Button ID="button7" runat="server" Text="2" OnClick="button7_Click" />
                 <asp:Button ID="button8" runat="server" Text="3" OnClick="button8_Click" />
                 <asp:Button ID="button9" runat="server" Text=" - " OnClick="button9_Click" />
                 <asp:Button ID="button33" runat="server" Text="+/-" OnClick="button33_Click" />
                 <asp:Button ID="button32" runat="server" Text="%" OnClick="button32_Click" />
                 <asp:Button ID="button27" runat="server" Text="1/x" OnClick="button27_Click" />

            </p>
               <p style="padding-left:13.8%">

                 <asp:Button ID="button14" runat="server" Text="AC" OnClick="button14_Click" />
                 <asp:Button ID="button1" runat="server" Text="0" OnClick="button1_Click" />
                 <asp:Button ID="button2" runat="server" Text="." OnClick="button2_Click" />
                 <asp:Button ID="button5" runat="server" Text="=" OnClick="button5_Click" />
                 <asp:Button ID="button3" runat="server" Text="+" OnClick="button3_Click" />
                 <asp:Button ID="button21" runat="server" Text="sqrt" OnClick="button21_Click" />
                 <asp:Button ID="button30" runat="server" Text="ln x" OnClick="button30_Click" />
                 <asp:Button ID="button29" Width="55"  runat="server" Text="log x" OnClick="button29_Click" />
            </p>
        </div>
    </section>
          </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2>Criado e mantido por <a href="http://jhonathansouza.com" rel="Copyright" target="_blank"> Jhonathan de Souza Soares       </a></h2>
</asp:Content>
