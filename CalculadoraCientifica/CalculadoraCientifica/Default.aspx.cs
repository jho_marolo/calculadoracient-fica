﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CalculadoraCientifica
{
    public partial class _Default : Page
    {

        string no1
        {
            get
            {
                object o = ViewState["no1"];
                if (o == null) return string.Empty ;
                return o.ToString();
            }
            set
            {
                ViewState["no1"] = value;
            }
        }
        string constfun
        {
            get
            {
                object o = ViewState["constfun"];
                if (o == null) return string.Empty ;
                return o.ToString();
            }
            set
            {
                ViewState["constfun"] = value;
            }
        }

        bool inputstatus
        {
            get
            {
                object o = ViewState["inputstatus"];
                if (o == null) return false;
                return (bool)o;
            }
            set
            {
                ViewState["inputstatus"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                no1 = "";
                textBox1.ReadOnly = true;
              
            }
           
        }

        protected void button1_Click(object sender, EventArgs e)
        {
            if (inputstatus == true)
            {
                textBox1.Text += button1.Text;
            }
            else
            {
                textBox1.Text = button1.Text;
                inputstatus = true;
            }
        }

        protected void button6_Click(object sender, EventArgs e)
        {
            if (inputstatus == true)
            {
                textBox1.Text += button6.Text;

            }
            else
            {
                textBox1.Text = button6.Text;
                inputstatus = true;
            }

        }

        protected void button7_Click(object sender, EventArgs e)
        {
            if (inputstatus == true)
            {
                textBox1.Text += button7.Text;

            }
            else
            {
                textBox1.Text = button7.Text;
                inputstatus = true;
            }
        }

        protected void button8_Click(object sender, EventArgs e)
        {
            if (inputstatus == true)
            {
                textBox1.Text += button8.Text;
            }
            else
            {
                textBox1.Text = button8.Text;
                inputstatus = true;
            }
        }

        protected void button11_Click(object sender, EventArgs e)
        {
            if (inputstatus == true)
            {
                textBox1.Text += button11.Text;
            }
            else
            {
                textBox1.Text = button11.Text;
                inputstatus = true;
            }
        }

        protected void button12_Click(object sender, EventArgs e)
        {
            if (inputstatus == true)
            {
                textBox1.Text += button12.Text;
            }
            else
            {
                textBox1.Text = button12.Text;
                inputstatus = true;
            }
        }

        protected void button13_Click(object sender, EventArgs e)
        {
            if (inputstatus == true)
            {
                textBox1.Text += button13.Text;

            }
            else
            {
                textBox1.Text = button13.Text;
                inputstatus = true;
            }
        }

        protected void button16_Click(object sender, EventArgs e)
        {
            if (inputstatus == true)
            {
                textBox1.Text += button16.Text;
            }
            else
            {
                textBox1.Text = button16.Text;
                inputstatus = true;
            }
        }

        protected void button17_Click(object sender, EventArgs e)
        {
            if (inputstatus == true)
            {
                textBox1.Text += button17.Text;
            }
            else
            {
                textBox1.Text = button17.Text;
                inputstatus = true;
            }
        }

        protected void button18_Click(object sender, EventArgs e)
        {
            if (inputstatus == true)
            {
                textBox1.Text += button18.Text;
            }
            else
            {
                textBox1.Text = button18.Text;
                inputstatus = true;
            }
        }

        protected void button2_Click(object sender, EventArgs e)
        {
            if (inputstatus == true)
            {
                textBox1.Text += button2.Text;
            }
            else
            {
                textBox1.Text = button2.Text;
                inputstatus = true;
            }
        }

        //Operador SOmar
        protected void button3_Click(object sender, EventArgs e)
        {
            no1 = textBox1.Text;
            textBox1.Text = "";
            constfun = "+";
        }

        //subtrair
        protected void button9_Click(object sender, EventArgs e)
        {
            no1 = textBox1.Text;
            textBox1.Text = "";
            constfun = "-";
        }

        //multiplicar
        protected void button10_Click(object sender, EventArgs e)
        {
            no1 = textBox1.Text;
            textBox1.Text = "";
            constfun = "*";
        }

        //dividir
        protected void button15_Click(object sender, EventArgs e)
        {
            no1 = textBox1.Text;
            textBox1.Text = "";
            constfun = "/";
        }

        //função definida pelo usuário
        protected void funcal()
        {
            switch (constfun)
            {
                case "+":
                    textBox1.Text = Convert.ToString(Convert.ToDouble(no1) + Convert.ToDouble(textBox1.Text));

                    break;
                case "-":
                    textBox1.Text = Convert.ToString(Convert.ToDouble(no1) - Convert.ToDouble(textBox1.Text));

                    break;
                case "*":
                    textBox1.Text = Convert.ToString(Convert.ToDouble(no1) * Convert.ToDouble(textBox1.Text));
                    break;
                case "/":
                    if (textBox1.Text == "0")
                    {
                        textBox1.Text = "infinito";
                    }
                    else
                    {
                        textBox1.Text = Convert.ToString(Convert.ToDouble(no1) / Convert.ToDouble(textBox1.Text));
                    }
                    break;
                case "x^y":
                    textBox1.Text = Convert.ToString(System.Math.Pow(Convert.ToDouble(no1), Convert.ToDouble(textBox1.Text)));
                    break;
                case "mod": textBox1.Text = Convert.ToString(Convert.ToDouble(no1) % Convert.ToDouble(textBox1.Text));
                    break;
                case "nPr":
                    Double varn, var2, var3;                      //declara variaveis
                    varn = factorial(Convert.ToSingle(no1));    //chama funcão factorial
                    var2 = factorial(Convert.ToSingle(no1) - Convert.ToSingle(textBox1.Text));
                    textBox1.Text = Convert.ToString(varn / var2);
                    break;
                case "nCr":
                    varn = factorial(Convert.ToSingle(no1));
                    var2 = factorial(Convert.ToSingle(no1) - Convert.ToSingle(textBox1.Text));
                    var3 = factorial(Convert.ToSingle(textBox1.Text));
                    textBox1.Text = Convert.ToString(varn / (var3 * var2));
                    break;
            }
        }

        //funcao para calcular o factorial
        private Single factorial(Single x)
        {
            Single i = 1;
            for (Single s = 1; s <= x; s++)
            {
                i = i * s;
            }
            return i;
        }

        protected void button5_Click(object sender, EventArgs e)
        {
            funcal();
            inputstatus = false;
        }

        //when AC is pressed to power on
        protected void button14_Click(object sender, EventArgs e)
        {
            textBox1.Enabled = true;
            textBox1.Text = "0";
        }

        //calculando x evelvado a 2
        protected void button19_Click(object sender, EventArgs e)
        {
            textBox1.Text = Convert.ToString(Convert.ToDouble(textBox1.Text) * Convert.ToDouble(textBox1.Text));
            inputstatus = false;
        }

        //calculando x elevado a 3
        protected void button20_Click(object sender, EventArgs e)
        {
            textBox1.Text = Convert.ToString(Convert.ToDouble(textBox1.Text) * Convert.ToDouble(textBox1.Text) * Convert.ToDouble(textBox1.Text));
            inputstatus = false;
        }

        //calculando raiz quadrada
        protected void button21_Click(object sender, EventArgs e)
        {
            textBox1.Text = Convert.ToString(System.Math.Sqrt(Convert.ToDouble(textBox1.Text)));
            inputstatus = false;
        }


        //x elevedo a y
        protected void button22_Click(object sender, EventArgs e)
        {
            no1 = textBox1.Text;
            textBox1.Text = "";
            constfun = "x^y";
        }

        //botão CE pressionado
        protected void button23_Click(object sender, EventArgs e)
        {
            textBox1.Text = String.Empty;
            inputstatus = true;
        }

        //setar valor de PI
        protected void button4_Click(object sender, EventArgs e)
        {
            textBox1.Text = "3.141592654";
        }

        //seno
        protected void button24_Click(object sender, EventArgs e)
        {
            //se selecionou radianos
            if (radiolist.SelectedValue == "Rad")
            {
                textBox1.Text = Convert.ToString(System.Math.Sin(Convert.ToDouble(textBox1.Text)));
                inputstatus = false;
            }
            //se selecionou graus
            else
            {
                textBox1.Text = Convert.ToString(System.Math.Sin((Convert.ToDouble(System.Math.PI) / 180) * (Convert.ToDouble(textBox1.Text))));
                inputstatus = false;
            }
        }

        //cosseno
        protected void button25_Click(object sender, EventArgs e)
        {
            //radianos
            if (radiolist.SelectedValue == "Rad")
            {
                textBox1.Text = Convert.ToString(System.Math.Cos(Convert.ToDouble(textBox1.Text)));
                inputstatus = false;
            }

                //graus
            else
            {
                textBox1.Text = Convert.ToString(System.Math.Cos((Convert.ToDouble(System.Math.PI) / 180) * (Convert.ToDouble(textBox1.Text))));
                inputstatus = false;
            }
        }


        //tangente
        protected void button26_Click(object sender, EventArgs e)
        {
            //radianos
            if (radiolist.SelectedValue == "Rad")
            {

                textBox1.Text = Convert.ToString(System.Math.Tan(Convert.ToDouble(textBox1.Text)));
                inputstatus = false;
            }
            //graus
            else
            {
                textBox1.Text = Convert.ToString(System.Math.Tan((Convert.ToDouble(System.Math.PI) / 180) * (Convert.ToDouble(textBox1.Text))));
                inputstatus = false;
            }
        }

        //calculando 1/x
        protected void button27_Click(object sender, EventArgs e)
        {
            textBox1.Text = Convert.ToString(Convert.ToDouble(1.0 / Convert.ToDouble(textBox1.Text)));
            inputstatus = false;
        }

        //calculando x! 
        protected void button28_Click(object sender, EventArgs e)
        {
            Single var1 = 1;
            Single valor = Convert.ToSingle(textBox1.Text);
            for (int i = 1; i <= valor; i++)
            {
                var1 = var1 * i;
            }
            textBox1.Text = Convert.ToString(var1);
            inputstatus = false;

        }

        //calculanado log10
        protected void button29_Click(object sender, EventArgs e)
        {
            textBox1.Text = Convert.ToString(System.Math.Log10(Convert.ToDouble(textBox1.Text)));
            inputstatus = false;
        }

        //calculando log natural
        protected void button30_Click(object sender, EventArgs e)
        {
            textBox1.Text = Convert.ToString(System.Math.Log(Convert.ToDouble(textBox1.Text)));
            inputstatus = false;
        }

        //pressionou backspace
        protected void button31_Click(object sender, EventArgs e)
        {
            no1 = textBox1.Text;
            int n = no1.Length;
            textBox1.Text = (no1.Substring(0, n - 1));
        }
     
        //operator(%) mod
        protected void button32_Click(object sender, EventArgs e)
        {
            no1 = textBox1.Text;
            textBox1.Text = "";
            constfun = "mod";
        }

        //codigo para chave +/- 
        protected void button33_Click(object sender, EventArgs e)
        {
            textBox1.Text = Convert.ToString(-Convert.ToInt32(textBox1.Text));
            inputstatus = false;
        }


        //inverso de Seno
        protected void button35_Click(object sender, EventArgs e)
        {
            if (radiolist.SelectedValue == "Rad")
            {
                textBox1.Text = Convert.ToString(System.Math.Asin(Convert.ToDouble(textBox1.Text)));
                inputstatus = false;
            }
            else
            {
                textBox1.Text = Convert.ToString(System.Math.Asin((Convert.ToDouble(System.Math.PI) / 180) * (Convert.ToDouble(textBox1.Text))));
                inputstatus = false;
            }
        }

        //Inversa do cosseno
        protected void button34_Click(object sender, EventArgs e)
        {
            if (radiolist.SelectedValue == "Rad")
            {
                textBox1.Text = Convert.ToString(System.Math.Acos(Convert.ToDouble(textBox1.Text)));
                inputstatus = false;
            }
            else
            {
                textBox1.Text = Convert.ToString(System.Math.Acos((Convert.ToDouble(System.Math.PI) / 180) * (Convert.ToDouble(textBox1.Text))));
                inputstatus = false;
            }
        }

        //Inversa da tangente
        protected void button36_Click(object sender, EventArgs e)
        {
            if (radiolist.SelectedValue == "Rad")
            {
                textBox1.Text = Convert.ToString(System.Math.Atan(Convert.ToDouble(textBox1.Text)));
                inputstatus = false;
            }
            else
            {
                textBox1.Text = Convert.ToString(System.Math.Atan((Convert.ToDouble(System.Math.PI) / 180) * (Convert.ToDouble(textBox1.Text))));
                inputstatus = false;
            }
        }


      
        //permutação
        protected void button37_Click(object sender, EventArgs e)
        {
            no1 = textBox1.Text;
            textBox1.Text = "";
            constfun = "nPr";
        }

        //combinação
        protected void button38_Click(object sender, EventArgs e)
        {
            no1 = textBox1.Text;
            textBox1.Text = "";
            constfun = "nCr";
        }

        //codigo para botão off 
        protected void button39_Click(object sender, EventArgs e)
        {
            textBox1.Enabled = false;
            textBox1.Text = "";
            inputstatus = false;
        }

     


    }
}